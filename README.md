## CK0031: Homework 01
This code was developed/improved by undergraduate computer science students at UFC - Brazil for course Artificial Intelligence - CK0031 - Prof. PhD Francesco Corona - 2016.2
It uses the AIMA core python project, with the new class amatrice.py and a few modifications in the agents.py class.

Team: Darley Barreto, Israel Vidal, Pedro Henrique, Yuri Barbosa